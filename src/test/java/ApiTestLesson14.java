import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.*;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matchers;
import org.testng.annotations.*;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class ApiTestLesson14 {

    RequestSpecification requestSpecBuilder;
    public static Map<String, Map<String, String>> map = new HashMap<String, Map<String, String>>();

//    public static Map<Object, Object> map = new HashMap<Object, Object>();

    @BeforeMethod
    public void init() {
        requestSpecBuilder = given().baseUri("http://wiremock.atlantis.t-systems.ru/")
                .filter(new RequestLoggingFilter())
                .filter(new ResponseLoggingFilter());
//                build();
        Map<String, String> mapService = new HashMap<String, String>();
        mapService.put("method", "GET");
        mapService.put("url", "akhodneva1/test");

        map.put("request", mapService);

        Map<String, String> map2 = new HashMap<String, String>();
        map2.put("status", "202");

        map.put("response", map2);
    }

    @Test
    public void updateMappingTest() {
        given((RequestSpecification) requestSpecBuilder)
                .body(map)
                .when()
//                .get("/akhodnev/test")
                .request("PUT", "/__admin/mappings/9a2b8535-569a-495a-a743-6902f2f26620")
                .then().assertThat().statusCode(200)
                .body("$", Matchers.hasEntry("name", "Anastasiia"))
                .header("Content-Type", "application/json");

    }


    @Test
    public void mappingTest() {
        given((RequestSpecification) requestSpecBuilder)
                .when()
//                .get("/akhodnev/test")
                .request("GET", "akhodnev/test")
                .then().assertThat().statusCode(200)
                .body("$", Matchers.hasEntry("name", "Anastasiia"))
                .header("Content-Type", "application/json");

    }
}